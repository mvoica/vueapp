
export const mockData = [
	{
		name: 'Album 1',
		id: 1,
		favorite: false,
		cover: 'https://u.scdn.co/images/pl/default/9420613945e1ab19cbc1bf2de85e315532b4b799',
		songs: [
			{
				id: 1,
				name: 'Song 1',
				artists: 'Drake',
				duration: 123 // seconds
			},
			{
				id: 2,
				name: 'Song 2',
				artists: 'Drake',
				duration: 123 // seconds
			}
		]
	},
	{
		name: 'Album 2',
		id: 2,
		favorite: false,
		cover: 'https://u.scdn.co/images/pl/default/9420613945e1ab19cbc1bf2de85e315532b4b799',
		songs: [
			{
				id: 1,
				name: 'Song 1',
				artists: 'Drake',
				duration: 123 // seconds
			},
			{
				id: 2,
				name: 'Song 2',
				artists: 'Drake',
				duration: 123 // seconds
			}
		]
	},
	{
		name: 'Album 3',
		id: 3,
		favorite: false,
		cover: 'https://u.scdn.co/images/pl/default/9420613945e1ab19cbc1bf2de85e315532b4b799',
		songs: [
			{
				id: 1,
				name: 'Song 1',
				artists: 'Drake',
				duration: 123 // seconds
			},
			{
				id: 2,
				name: 'Song 2',
				artists: 'Drake',
				duration: 123 // seconds
			}
		]
	},
	{
		name: 'Album 4',
		id: 4,
		favorite: true,
		cover: 'https://u.scdn.co/images/pl/default/9420613945e1ab19cbc1bf2de85e315532b4b799',
		songs: [
			{
				id: 1,
				name: 'Song 1',
				artists: 'Drake',
				duration: 123 // seconds
			},
			{
				id: 2,
				name: 'Song 2',
				artists: 'Drake',
				duration: 123 // seconds
			}
		]
	},
	{
		name: 'Album 5',
		id: 5,
		favorite: true,
		cover: 'https://u.scdn.co/images/pl/default/9420613945e1ab19cbc1bf2de85e315532b4b799',
		songs: [
			{
				id: 1,
				name: 'Song 1',
				artists: 'Drake',
				duration: 123 // seconds
			},
			{
				id: 2,
				name: 'Song 2',
				artists: 'Drake',
				duration: 123 // seconds
			}
		]
	},
	{
		name: 'Album 6',
		id: 6,
		favorite: false,
		cover: 'https://u.scdn.co/images/pl/default/9420613945e1ab19cbc1bf2de85e315532b4b799',
		songs: [
			{
				id: 1,
				name: 'Song 1',
				artists: 'Drake',
				duration: 123 // seconds
			},
			{
				id: 2,
				name: 'Song 2',
				artists: 'Drake',
				duration: 123 // seconds
			}
		]
	}
]
