## Requirements ##

Implement a Spotify player look-alike, with a focus on the following features:

- Browse albums as thumbnails
- Search for an album
- Select an album to see its details and tracklist
- Star an album
- Display a list of starred albums
- Create a playlist

No song playing has to be implemented.
You can use dummy data for everything.


## Explanations ##

- I took the entire css from Spotify (because 1: css is not my strongest suit, and 2: somebody already made all the effort around making that site look good, and 3: even though the assignment was not about how things look - to me, UX experience comes first)
- The js framework I feel most comfortable with is knockout.js. However, requirements also asked for components, routing and state management, which is well, a pattern that maps to react quite well. To do this in knockout I would have had to plug in something for state management, and something for routing (probably Sammy.js) + the code to hold them together. 
- I have worked little with npm before (to the extent of running gulp tasks for bundling on bamboo). The server I feel most comfortable with is IIS (+ .net C# backend - no .net core experience though). Making webpack work with visual studio build process is not as easy as I thought.
- Therefore, I took the opportunity to learn the basics of a framework that is "similar" to react in... < 8hrs. Enter vue.js (which also has a nice cli tool for bootstrapping the build setup). I didn't choose react, because it seemed I would need more time to digest it.
- I had huge issues with the compatibility between the editor of choice (intellij webstorm) and the linting profile I wanted (with tabs instead of spaces). Since I didn't figure out what the incompatibility is (looked in webstorm settings for a while) I had to disable the linting on some of the files.