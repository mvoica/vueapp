/* eslint-disable no-mixed-spaces-and-tabs,indent */
import Vue from 'vue'
import Vuex from 'vuex'
import { mockData } from './mockData'

Vue.use(Vuex)

export const store = new Vuex.Store({
	state: {
		albums: mockData
	},
	mutations: {
		MARK_ALBUM_AS_FAVORITE (state, album) {
			album.favorite = true
		},
		UNMARK_ALBUM_AS_FAVORITE (state, album) {
			album.favorite = false
		}
	},
	actions: {
		markAlbumAsFavorite ({commit}, album) {
			commit('MARK_ALBUM_AS_FAVORITE', album)
		},
		unmarkAlbumAsFavorite ({commit}, album) {
			commit('UNMARK_ALBUM_AS_FAVORITE', album)
		}
	},
	getters: {
		albums: state => state.albums,
		favoriteAlbums: state => state.albums.filter((album) => { return album.favorite }),
		albumById: state => (id) => {
      return state.albums.find((album) => { return album.id.toString() === id.toString() })
		},
		trackPosition: state => (track) => {
			var album = state.albums.find((album) => { return album.tracks.indexOf(track) !== -1 })
			return album.tracks.indexOf(track) + 1 // we know we have it, because we checked above
		}
	}
})
