import Vue from 'vue'
import Router from 'vue-router'
import View1 from '@/components/View1'
import View2 from '@/components/View2'
import View3 from '@/components/View3'

Vue.use(Router)

export const router = new Router({
	routes: [
		{
			path: '/',
			name: 'View1',
			component: View1
		},
		{
			path: '/favorites',
			name: 'View2',
			component: View2
		},
		{
			path: '/album/:id',
			name: 'View3',
			component: View3
		}
	]
})
